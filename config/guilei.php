<?php

return [
    'postUrl' => env('POST_URL'),
    'homePath' => env('HOME_PATH'),
    'picPath' => env('PIC_PATH'),
    'categories' => [
        1 => [
            'name' => '二极管',
            'pinyin' => 'https://www.sitongzixun.com/channel_product/i8723.html',
        ],
    ]
];
