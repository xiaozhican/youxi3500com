<?php

namespace App\Console\Commands;

use App\Models\Config;
use App\Models\Sell;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Nesk\Puphpeteer\Puppeteer;
use Symfony\Component\DomCrawler\Crawler;

class Guilei extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'guilei';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '爬虫';
    protected $host = "https://www.baizhan.net";

    private Client $client;

    public array $config;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client([
            'timeout' => 10,
        ]);
        $dbConfig = Config::all()->toArray();
        $this->config = array_merge(['xpath' => array_column($dbConfig, 'value', 'key')], config('guilei'));
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws GuzzleException
     * @throws Exception
     */
    public function handle(): void
    {
        file_put_contents('./guilei.txt',"\n".date('Y-m-d H:i:s').'---开始采集---'."\n",FILE_APPEND);
        $this->zixun();
        //$this->game();
        //$this->danji();
    }

    public function zixun()
    {
        //$link = "http://www.3500.com/zixun/xinwen.html";
        $categorys = DB::table('guilei')->where('id','<=',12)->get();
        $categorys  =json_decode($categorys,true);
        foreach ($categorys as $category){
            $link = $category['url_address'];
            $classid = $category['classid'];
            $options = [
                'http_errors' => true,
                'force_ip_resolve' => 'v4',
                'connect_timeout' => 0,
                'read_timeout' => 0,
                'timeout' => 0,
            ];
            $response = $this->client->get($link,$options);
            $content = $response->getBody()->getContents();
            //var_dump($content);
            $crawler = new Crawler($content);
            $page = $crawler->filterXPath("//html/body/div[3]/div/div[2]/div[2]/a[9]")->text();
            $page = $this->replacePrice($page);
            for($i=1;$i<=1;$i++){
                if($i==1){
                    $url = $link;
                }else {
                    $url = $category['url_address']."index_{$i}.html";
                }
                //$url = $i==1?$link:"http://www.3500.com/zixun/xinwen-{$i}.html";
                $list = $this->getZixun($url,$category);
                if ($list) {
                    echo '，需要更新'.count($list).'篇'.PHP_EOL;
                } else {
                    echo '，没有需要更新的'.PHP_EOL;
                    continue;
                }

                foreach ($list as $key=>$item){
                    //var_dump($item);
                    if($item==NULL){
                        continue;
                    }
                    $pageContent = $this->page($this->host.$item['url']);
                    //var_dump($pageContent['addr']);
                    if ($pageContent === false) {
                        exit('子页面抓取错误');
                    }
                    $list[$key] = array_merge($pageContent, $item);
                    $this->post($classid,$list[$key]);
                    //var_dump($pageContent['info']);
//                    if(empty($pageContent['tag'])){
//                        $tag = $item['title'];
//                    }else{
//                        $ex = explode(',',$pageContent['tag']);
//                        $tag = $ex[0];
//                    }
/*
                    $id = DB::table('www_moban5_cn_ecms_news')->insertGetId([
                        'classid' => $classid,
                        'ttid' => 0,
                        'onclick' => 0,
                        'plnum' => 0,
                        'totaldown' => 0,
                        'userid' => 1,
                        'username' => 'moban5',
                        'firsttitle' => 0,
                        'isgood' => 0,
                        'ispic' => 1,
                        'istop' => 0,
                        'isqf' => 0,
                        'ismember' => 0,
                        'isurl' => 0,
                        'truetime' => time(),
                        'lastdotime' => time(),
                        'havehtml' => 1,
                        'groupid' => 0,
                        'userfen' => 0,
                        'stb' => 1,
                        'fstb' => 1,
                        'restb' => 1,
                        'keyboard' => $tag,
                        'title' => $item['title'],
                        'newstime' => time(),
                        'titlepic' => $item['img'],
                        'smalltext' => $item['desc'],
                        'diggtop' => 0,
                        //'xgname' => $item['title'],
                        'eckuid' => 0
                    ]);
                    DB::table('www_moban5_cn_ecms_news')->where('id',$id)->update([
                        'filename' => $id,
                        'titleurl' => "https://www.guilei.com".$category['titleurl']."/{$id}.html"
                    ]);
                    DB::table('www_moban5_cn_ecms_news_data_1')->insert([
                        'id' =>$id,
                        'classid' => $classid,
                        'keyid' => $id,
                        'dokey' => 0,
                        'newstempid' => 0,
                        'closepl' => 0,
                        'haveaddfen' => 0,
                        'infotags' => $tag,
                        //'writer' => 'usewirten',
                        'newstext' => $pageContent['info']
                    ]);
                    file_put_contents('./guilei.txt',$id."，",FILE_APPEND);
*/
                }
            }
        }

    }
    //咨询-新闻
    public function getZixun($url,$category)
    {
        try{
            //$url = "http://www.3500.com/zixun/xinwen-6.html";
            $options = [
                'http_errors' => true,
                'force_ip_resolve' => 'v4',
                'connect_timeout' => 0,
                'read_timeout' => 0,
                'timeout' => 0,
            ];
            $response = $this->client->get($url,$options);
            $content = $response->getBody()->getContents();
            //var_dump($content);
            $crawler = new Crawler($content);
            return $crawler->filterXPath($category['list'])->each(function (Crawler $cr) use ($category){
                $title = $cr->filter($category['name'])->text();//标题
                $url = $cr->filter($category['content_url'])->attr('href');
                $desc = $cr->filter($category['desc'])->text();
                $img = $cr->filter($category['img'])->attr('src');
                //$img = $this->download($img);
                //var_dump($url);
                return [
                    'title' => $this->replaceAll($title),
                    'url' => $url,
                    'desc' => $desc,
                    'img' => $img,
                ];
            });
        }catch (Exception $e) {
            Log::info(date('Y-m-d H:i:s')."---{$url}页面错误，403了----");
            \Log::error($e->getMessage());
        }
        return [];
    }
    public function zixun1()
    {
        //$link = "http://www.3500.com/zixun/xinwen.html";
        $categorys = DB::table('category')->where('id',1)->get();
        $categorys  =json_decode($categorys,true);
        foreach ($categorys as $category){
            $link = $category['linkurl'];
            $classid = $category['classid'];
            $response = $this->client->get($link);
            $content = $response->getBody()->getContents();
            //var_dump($content);
            $crawler = new Crawler($content);
            $page = $crawler->filterXPath("//html/body/div[3]/div/div[2]/div[2]/a[9]")->text();
            $page = $this->replacePrice($page);
            for($i=1;$i<=$page;$i++){
                if($i==1){
                    $url = $link;
                }else {
                    $url = $category['linkurl']."index_{$i}.html";
                }
                //$url = $i==1?$link:"http://www.3500.com/zixun/xinwen-{$i}.html";
                $list = $this->getZixun($url);
                if ($list) {
                    echo '，需要更新'.count($list).'篇'.PHP_EOL;
                } else {
                    echo '，没有需要更新的'.PHP_EOL;
                    continue;
                }

                foreach ($list as $item){
                    if($item==NULL){
                        continue;
                    }
                    $pageContent = $this->page($this->host.$item['url']);
                    //var_dump($pageContent['addr']);
                    if ($pageContent === false) {
                        exit('子页面抓取错误');
                    }
                    //var_dump($pageContent['info']);
                    if(empty($pageContent['tag'])){
                        $tag = $item['title'];
                    }else{
                        $ex = explode(',',$pageContent['tag']);
                        $tag = $ex[0];
                    }

                    $id = DB::table('www_moban5_cn_ecms_news')->insertGetId([
                        'classid' => $classid,
                        'ttid' => 0,
                        'onclick' => 0,
                        'plnum' => 0,
                        'totaldown' => 0,
                        'userid' => 1,
                        'username' => 'moban5',
                        'firsttitle' => 1,
                        'isgood' => 0,
                        'ispic' => 1,
                        'istop' => 0,
                        'isqf' => 0,
                        'ismember' => 0,
                        'isurl' => 0,
                        'truetime' => time(),
                        'lastdotime' => time(),
                        'havehtml' => 1,
                        'groupid' => 0,
                        'userfen' => 0,
                        'stb' => 1,
                        'fstb' => 1,
                        'restb' => 1,
                        'keyboard' => $tag,
                        'title' => $item['title'],
                        'newstime' => time(),
                        'titlepic' => $item['img'],
                        'smalltext' => $item['desc'],
                        'diggtop' => 0,
                        //'xgname' => $item['title'],
                        'eckuid' => 0
                    ]);
                    DB::table('www_moban5_cn_ecms_news')->where('id',$id)->update([
                        'filename' => $id,
                        'titleurl' => "https://www.guilei.com".$category['titleurl']."/{$id}.html"
                    ]);
                    DB::table('www_moban5_cn_ecms_news_data_1')->insert([
                        'id' =>$id,
                        'classid' => $classid,
                        'keyid' => $id,
                        'dokey' => 0,
                        'newstempid' => 0,
                        'closepl' => 0,
                        'haveaddfen' => 0,
                        'infotags' => $tag,
                        //'writer' => 'usewirten',
                        'newstext' => $pageContent['info']
                    ]);
                    file_put_contents('./guilei.txt',$id."，",FILE_APPEND);
                }
                file_put_contents('./guilei.txt',$category['site_name']."\n",FILE_APPEND);
            }
        }

    }

    public function guilei()
    {
        /**
         * return $crawler->filterXPath("//div[@class='main']/div/ul/li")->each(function (Crawler $cr){
        $title = $cr->filter('.title')->text();//标题
        $url = $cr->filter('.con a')->attr('href');
        $desc = $cr->filterXPath("//p")->nextAll()->text();//html/body/div[5]/div[3]/ul/li[1]/div[2]/p[2]
        $img = $cr->filter('.img a img')->attr('src');
         */
        $categorys = DB::table('category')->get();
        foreach ($categorys as $category){
            DB::table('guilei')->insert([
                'name' => 'h5 a',
                'content_url' => 'h5 a',
                'desc'  => '.bq_hd',
                'img' => 'img',
                'list' => "//html/body/div[3]/div/div[2]/div[1]/ul/li",
                'url_address' => $category->linkurl,
                'site_name' => $category->site_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'site_type' => $category->classid,
                'classid' => $category->classid,
                'titleurl' => $category->titleurl
            ]);
        }

    }

    /**
     * @param  string  $url
     * @return array|false
     * @throws GuzzleException
     */
    public function page(string $url)
    {
        $info = '';
        try {
            $response = $this->client->request('GET', $url);
            $content = $response->getBody()->getContents();
            $crawler = new Crawler($content);
            if (preg_match('/<meta name="keywords" content="(?<tags>.*?)"/u', $content, $matches) === false) {
                return false;
            }
            //$content = $crawler->filterXPath($this->config['xpath']['body'])->html();
            if($crawler->filter('.article')->count()==1){
                $info = $crawler->filter('.article')->html();
            }else{
                $info = '';
            }

            //var_dump($info);
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }
        return [
            'tags' => empty($matches)?'':$this->replaceAll($matches['tags']),
            'info' => $info,
        ];
    }

    public function post(int $classid, $post): void
    {
//        preg_match_all('/<img src="(.*?)"/', $post['content'], $matches);
//        foreach ($matches[1] as $item) {
//            $post['content'] = str_replace($item, $this->picDown('https://www.qiqu.net'.$item),
//                $post['content']);
//        }
//        $post['content'] = preg_replace('/<a.*?\/a>/', '', $post['content']);
        $resp = $this->client->post("https://www.guilei.com/e/admin-guilei/EcmsLogin.php?pw=www.moban5.cn", [
            'form_params' => [
                'title' => $post['title'],
                'newstext' => $post['info'],
                'classid' => $classid,
                'username' => 'moban5',
                'oldchecked' => 1,
                'newstime' => time(),
                'checked' => 1,
                //'morepic' =>$post['img'],
                'smalltext' => $post['desc'],
                'titlepic' => $post['img'],
                'isgood' => rand(0,1),
                'keyboard' => $post['tags'],
                'infotags' => $post['tags']
            ],
        ]);
        //var_dump($resp->getBody()->getContents());
        if (strpos($resp->getBody()->getContents(), '增加信息成功') === false) {
            echo $resp->getBody()->getContents();
            exit;
        }
    }
    public function pic()
    {
        //$link = "http://www.3500.com/zixun/xinwen.html";
        $categorys = DB::table('guilei')->where('id','>=',13)->get();
        $categorys  =json_decode($categorys,true);
        foreach ($categorys as $category){
            $link = $category['url_address'];
            $classid = $category['classid'];
            $options = [
                'http_errors' => true,
                'force_ip_resolve' => 'v4',
                'connect_timeout' => 0,
                'read_timeout' => 0,
                'timeout' => 0,
            ];
            //$response = $this->client->get($link,$options);
            //$content = $response->getBody()->getContents();
            //var_dump($content);
            //$crawler = new Crawler($content);
            //$page = $crawler->filterXPath("//html/body/div[3]/div/div[2]/div[2]/a[9]")->text();
            //$page = $this->replacePrice($page);
            for($i=1;$i<=1;$i++){
                if($i==1){
                    $url = $link;
                }else {
                    $url = $category['url_address']."index_{$i}.html";
                }
                //$url = $i==1?$link:"http://www.3500.com/zixun/xinwen-{$i}.html";
                $list = $this->getZixun($url,$category);
                if ($list) {
                    echo '，需要更新'.count($list).'篇'.PHP_EOL;
                } else {
                    echo '，没有需要更新的'.PHP_EOL;
                    continue;
                }

                foreach ($list as $key=>$item){
                    //var_dump($item);
                    if($item==NULL){
                        continue;
                    }
                    $pageContent = $this->page($this->host.$item['url']);
                    //var_dump($pageContent['addr']);
                    if ($pageContent === false) {
                        exit('子页面抓取错误');
                    }
                    $list[$key] = array_merge($pageContent, $item);
                    $this->picPost($classid,$list[$key]);
                }
            }
        }

    }
    public function picPost(int $classid, $post): void
    {
//        preg_match_all('/<img src="(.*?)"/', $post['content'], $matches);
//        foreach ($matches[1] as $item) {
//            $post['content'] = str_replace($item, $this->picDown('https://www.qiqu.net'.$item),
//                $post['content']);
//        }
//        $post['content'] = preg_replace('/<a.*?\/a>/', '', $post['content']);

        $resp = $this->client->post("https://www.guilei.com/e/admin-guilei/Ecmsphoto.php?pw=www.moban5.cn", [
            'form_params' => [
                'title' => $post['title'],
                //'newstext' => $post['info'],
                'classid' => $classid,
                'username' => 'moban5',
                'oldchecked' => 1,
                'newstime' => date('Y-m-d'),
                'checked' => 1,
                'smalltext' => $post['desc'],
                'titlepic' => $post['img'],
                'isgood' => rand(0,1),
                'keyboard' => $post['tags'],
                'infotags' => $post['tags'],
                'mpictures' => $post['img'],
                'pictures' => $post['img'],
                'picnext' => '图片'
            ],
        ]);
        if (strpos($resp->getBody()->getContents(), '增加信息成功') === false) {
            echo $resp->getBody()->getContents();
            exit;
        }
    }

    /**
     * @param  string  $word  需要替换的字符串
     * @return string
     */
    public function replaceIntro(string $word): string
    {
        $word = str_ireplace('www.QiQu.net ', 'cpadol.com', $word);

        return str_replace('奇趣网', '天下奇闻网', $word);
    }

    public function replacePrice($word)
    {
        preg_match('#([+-]?\d+(\.\d+)?)#',$word,$matches);//提取数字
        return $matches[0];
    }
    public function replaceImg($word)
    {
        if($word=="//static.huangye88.cn/images/none.gif"){
            $word = "http://oss.huangye88.net/live/user/826336/1536200234001363900-0.jpg@1e_1c_98w_98h_90Q";
        }
        return $word;
    }
    public function replaceAll($word)
    {
        if($word=='') return $word;
        $replace = DB::table('replace')->where('id',1)->select('content')->first();
        $contents = explode("\r\n",$replace->content);
        foreach ($contents as $key=>$content){
            $re = explode('||',$content);
            if(!isset($re[0]) || !isset($re[1])){
                continue;
            }
            //var_dump($re[1]);
            if(strpos($word,$re[0])!==false){
                $word = str_replace($re[0],$re[1],$word);
                continue;
            }
        }
        return $word;
    }

    public function download($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file = curl_exec($ch);
        curl_close($ch);
        //$filename = pathinfo($url, PATHINFO_BASENAME);
        $timePath = date('Ymd');
        $h = date('H');
        $picName = $this->randStr(11);

        if (preg_match('/^.*\.(?<extension>.*?)$/', $url, $matches) === false) {
            exit('图片扩展名获取失败');
        }
        $file = "/home/www/guilei.com/d/file/titlepic/".$timePath.'/'.$h;
        if(!file_exists($file)){
            mkdir($file,0777,true);
            chmod($file,0777);
        }
        $path = $timePath.'/'.$h.'/'.$picName.'.'.$matches['extension'];
        $resource = fopen("/home/www/guilei.com/d/file/titlepic/".$path, 'a');
        fwrite($resource, $file);
        fclose($resource);
        return 'https://www.guilei.com/d/file/titlepic/'.$path;
    }

    /**
     * @param  string  $url
     * @return string
     * @throws Exception
     */
    public function picDown(string $url): string
    {
        $timePath = date('YmdHis');
        $picName = $this->randStr(11);

        if (preg_match('/^.*\.(?<extension>.*?)$/', $url, $matches) === false) {
            exit('图片扩展名获取失败');
        }

        $path = $timePath.'/'.$picName.'.'.$matches['extension'];

        Storage::put($path, file_get_contents($url));

        //return config('app.url').config('youxi3500com.picPath').'/'.$path;
        return $path;
    }

    /**
     * @param  int  $length
     * @return string
     * @throws Exception
     */
    public function randStr(int $length): string
    {
        $str = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $len = strlen($str) - 1;
        $randStr = '';
        for ($i = 0; $i < $length; $i++) {
            $num = random_int(0, $len);
            $randStr .= $str[$num];
        }

        return $randStr;
    }
}
