<?php

namespace App\Console\Commands;

use App\Models\Config;
use App\Models\Sell;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Nesk\Puphpeteer\Puppeteer;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\DomCrawler\Crawler;

class Youxi3500com extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'youxi3500com';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '爬虫';

    private Client $client;

    public array $config;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client([
            'timeout' => 10,
        ]);
        $dbConfig = Config::all()->toArray();
        $this->config = array_merge(['xpath' => array_column($dbConfig, 'value', 'key')], config('youxi3500com'));
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws GuzzleException
     * @throws Exception
     */
    public function handle(): void
    {
        file_put_contents('./youxi3500com.txt',"\n".date('Y-m-d H:i:s').'---开始采集---'."\n",FILE_APPEND);
        //$this->zixun();
        $this->game();
        //$this->danji();
    }

    /**
     * @param  array  $category
     * @return array
     * @throws GuzzleException
     */
    public function list($catid,$pinyin,$name): array
    {
        //sleep(3);
        try {
            $url = "{$pinyin}";
            //$url = "{$category}";
            //composer require nesk/puphpeteer
            //npm install @nesk/puphpeteer
//            $puppeteer = new Puppeteer();
//            //const puppeteer = require('puppeteer');
//            $browser = $puppeteer->launch([
//                'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
//            ]);
//            $page = $browser->newPage();
//            $page->goto($url,[
//                'timeout' => 0,
//                'read_timeout' => 0,
//                'idle_timeout' => 0
//            ]);
//            $html = $page->content();
//            //var_dump($html);
//            $browser->close();
//            //$response = $this->client->get($url);//get($url);
//            //$content = $response->getBody()->getContents();
//            //var_dump($content);
//            $crawler = new Crawler();
//            $crawler->addHtmlContent($html);
            Log::info(date('Y-m-d H:i:s')."---{$name}/{$url}成功----");
            $response = $this->client->get($url);
            $content = $response->getBody()->getContents();
            //var_dump($content);
            $crawler = new Crawler($content);
//            $xpath = $this->config['xpath'];
            //var_dump($xpath);die;

            return $crawler->filterXPath('//*/table/tbody/tr')->each(function (Crawler $node) use ($catid) {
                if($node->filter('tr')->children('.c_name')->count()==1){
                    //var_dump($i);
                    $title = $node->filterXPath('//p')->text();
                    if($node->filter('.c_lor')->children('span')->count()==0){
                        $price = 1000;
                    }else{
                        $price = $node->filter('.c_lor')->children('span')->text();
                    }

                    $url = $node->filter('.c_cont p a')->attr('href');
                    //var_dump($node->filter('.c_name a img')->attr('data-original'));
                    $thumb = $node->filter('.c_name a img')->attr('data-original');
                    //var_dump($title);
                    return [
                        'title' => $title,
                        //'intro' => $this->replaceIntro($intro),///html/body/div[3]/div[1]/div[2]/ul/li[5]/div[1]/a/img
                        'price' =>$this->replacePrice($price),
                        'url' => $url,
                        'thumb' => $this->replaceImg($thumb),
                        'catid' => $catid
                    ];
                }
            });
        } catch (Exception $e) {
            Log::info(date('Y-m-d H:i:s')."---{$name}/{$url}页面错误，403了----");
            \Log::error($e->getMessage());
        }
        return [];
    }

    public function zixun()
    {
        //$link = "http://www.3500.com/zixun/xinwen.html";
        $categorys = DB::table('youxi')->where('id',1)->get();
        $categorys  =json_decode($categorys,true);
        foreach ($categorys as $category){
            $link = $category['url_address'];
            $options = [
                'http_errors' => true,
                'force_ip_resolve' => 'v4',
                'connect_timeout' => 0,
                'read_timeout' => 0,
                'timeout' => 0,
            ];
            $response = $this->client->get($link,$options);
            $content = $response->getBody()->getContents();
            //var_dump($content);
            $crawler = new Crawler($content);
            $page = $crawler->filter('.a1')->text();
            $page = $this->replacePrice($page);
            if ($category['site_name']=='新闻'){
                $classid=2;
            }elseif ($category['site_name']=='攻略'){
                $classid=3;
            }elseif ($category['site_name']=='活动'){
                $classid=4;
            }elseif ($category['site_name']=='公告'){
                $classid=5;
            }
            for($i=1;$i<=1;$i++){
                if($i==1){
                    $url = $link;
                }elseif ($category['site_name']=='新闻'){
                    $url = "http://www.3500.com/zixun/xinwen-{$i}.html";
                }elseif ($category['site_name']=='攻略'){
                    $url = "http://www.3500.com/zixun/gonglue-{$i}.html";
                }elseif ($category['site_name']=='活动'){
                    $url = "http://www.3500.com/zixun/huodong-{$i}.html";
                }elseif ($category['site_name']=='公告'){
                    $url = "http://www.3500.com/zixun/gonggao-{$i}.html";
                }
                //$url = $i==1?$link:"http://www.3500.com/zixun/xinwen-{$i}.html";
                $list = $this->getZixun($url,$category);
                if ($list) {
                    echo '，需要更新'.count($list).'篇'.PHP_EOL;
                } else {
                    echo '，没有需要更新的'.PHP_EOL;
                    continue;
                }

                foreach ($list as $key=>$item){
                    if($item==NULL){
                        continue;
                    }
                    $pageContent = $this->page($item['url']);
                    //var_dump($pageContent['addr']);
                    if ($pageContent === false) {
                        exit('子页面抓取错误');
                    }
                    //var_dump($pageContent['info']);
                    //$tag = explode(',',$pageContent['tag']);
                    $list[$key] = array_merge($pageContent, $item);
                    $this->post($classid,$list[$key]);
/*
                    $id = DB::table('www_kaifamei_com_ecms_news')->insertGetId([
                        'classid' => $classid,
                        'ttid' => 0,
                        'onclick' => 0,
                        'plnum' => 0,
                        'totaldown' => 0,
                        'userid' => 1,
                        'username' => 'kaifamei',
                        'firsttitle' => 0,
                        'isgood' => 0,
                        'ispic' => 1,
                        'istop' => 0,
                        'isqf' => 0,
                        'ismember' => 0,
                        'isurl' => 0,
                        'truetime' => time(),
                        'lastdotime' => time(),
                        'havehtml' => 1,
                        'groupid' => 0,
                        'userfen' => 0,
                        'stb' => 1,
                        'fstb' => 1,
                        'restb' => 1,
                        'keyboard' => $tag[0],
                        'title' => $item['title'],
                        'newstime' => time(),
                        'titlepic' => $item['img'],
                        'smalltext' => $item['desc'],
                        'diggtop' => 0,
                        'xgname' => $item['title'],
                        'eckuid' => 0
                    ]);
                    DB::table('www_kaifamei_com_ecms_news')->where('id',$id)->update([
                        'filename' => $id,
                        'titleurl' => "/zixun/{$id}.html"
                    ]);
                    DB::table('www_kaifamei_com_ecms_news_data_1')->insert([
                        'id' =>$id,
                        'classid' => $classid,
                        'keyid' => $id,
                        'dokey' => 0,
                        'newstempid' => 0,
                        'closepl' => 0,
                        'haveaddfen' => 0,
                        'infotags' => $tag[0],
                        'writer' => 'usewirten',
                        'newstext' => $pageContent['info']
                    ]);
                    file_put_contents('./youxi3500com.txt',$id."，",FILE_APPEND);
*/
                }
            }
        }

    }
    //咨询-新闻
    public function getZixun($url,$category)
    {
        try{
            //$url = "http://www.3500.com/zixun/xinwen-6.html";
            $options = [
                'http_errors' => true,
                'force_ip_resolve' => 'v4',
                'connect_timeout' => 0,
                'read_timeout' => 0,
                'timeout' => 0,
            ];
            $response = $this->client->get($url,$options);
            $content = $response->getBody()->getContents();
            //var_dump($content);
            $crawler = new Crawler($content);
            return $crawler->filterXPath($category['list'])->each(function (Crawler $cr) use ($category){
                $title = $cr->filter($category['name'])->text();//标题
                $url = $cr->filter($category['content_url'])->attr('href');
                $desc = $cr->filter($category['desc'])->text();
                $img = $cr->filter($category['img'])->attr('src');
                $img = $this->download($img);
                return [
                    'title' => $title,
                    'url' => $url,
                    'desc' => $desc,
                    'img' => $img,
                ];
            });
        }catch (Exception $e) {
            Log::info(date('Y-m-d H:i:s')."---{$url}页面错误，403了----");
            \Log::error($e->getMessage());
        }
    }
    public function post(int $classid, $post): void
    {
//        preg_match_all('/<img src="(.*?)"/', $post['content'], $matches);
//        foreach ($matches[1] as $item) {
//            $post['content'] = str_replace($item, $this->picDown('https://www.qiqu.net'.$item),
//                $post['content']);
//        }
//        $post['content'] = preg_replace('/<a.*?\/a>/', '', $post['content']);

        $resp = $this->client->post($this->config['postUrl'], [
            'form_params' => [
                'classid' => $classid,
                'username' => 'kaifamei',
                'title' => $post['title'],
                'titlepic' => $post['img'],
                'newstime' => '',
                'checked' => 1,
                'infotags' => $post['tags'],
                'newstext' => $post['info'],
                'keyboard' => $post['tags'],
                'getmark' => 1,
                'smalltext' => $post['desc'],
                'writer' => $post['writer'],
                'xgname' => $post['xgname']
            ],
        ]);
//        $content = iconv("GBK", "UTF-8", $resp->getBody());
//        echo $content;
//        exit;
        if (strpos($resp->getBody()->getContents(), '增加信息成功') === false) {
            echo $resp->getBody()->getContents();
            exit;
        }
    }

    public function game()
    {
        //$url = "http://www.3500.com/newgame.html";
        $categorys = DB::table('youxi')->where('id',5)->get();
        $categorys  =json_decode($categorys,true);
        foreach ($categorys as $category){
            $link = $category['url_address'];
            $list = $this->getGame($link,$category);
            if ($list) {
                echo '，需要更新'.count($list).'篇'.PHP_EOL;
            } else {
                echo '，没有需要更新的'.PHP_EOL;
                continue;
            }

            foreach ($list as $key=>$item){
                if($item==NULL){
                    continue;
                }
                //var_dump($item['url']);

                $pageContent = $this->gamePage($item['url']);
                //var_dump($pageContent['addr']);
                if ($pageContent === false) {
                    exit('子页面抓取错误');
                }
                //var_dump($pageContent['info']);
                //$tag = explode(',',$pageContent['tag']);
//                $pics = $this->gamePic($item['url']);
//                $str = '';
//                foreach ($pics as $pic){
//                    $str.=$pic['picStr'].'::::::';
//                }
                $list[$key] = array_merge($pageContent, $item);
                $classid=6;
                $this->gamePost($classid,$list[$key]);
/*
                $id = DB::table('www_kaifamei_com_ecms_game')->insertGetId([
                    'classid' => 6,
                    'ttid' => 0,
                    'onclick' => 0,
                    'plnum' => 0,
                    'totaldown' => 0,
                    'userid' => 1,
                    'username' => 'kaifamei',
                    'firsttitle' => 1,
                    'isgood' => 0,
                    'ispic' => 1,
                    'istop' => 0,
                    'isqf' => 0,
                    'ismember' => 0,
                    'isurl' => 0,
                    'truetime' => time(),
                    'lastdotime' => time(),
                    'havehtml' => 1,
                    'groupid' => 0,
                    'userfen' => 0,
                    'stb' => 1,
                    'fstb' => 1,
                    'restb' => 1,
                    'keyboard' => $tag[0],
                    'title' => $item['title'],
                    'newstime' => time(),
                    'titlepic' => $item['img'],
                    'ftitle'=>$tag[0],
                    'smalltext' => $item['desc'],
                    'diggtop' => 0,
                    'fenlei' =>rand(1,8),
                    'yijuhua' =>$item['title'],
                    'morepic' => $str,
                    'h5url'=>$item['url'],
                    'eckuid' => 0
                ]);
                DB::table('www_kaifamei_com_ecms_game')->where('id',$id)->update([
                    'filename' => $id,
                    'titleurl' => "/game/{$id}.html"
                ]);
                DB::table('www_kaifamei_com_ecms_game_data_1')->insert([
                    'id' =>$id,
                    'classid' => 6,
                    //'keyid' => $id,
                    'dokey' => 0,
                    'newstempid' => 0,
                    'closepl' => 0,
                    'haveaddfen' => 0,
                    //'infotage' => $tag[0],
                    //'writer' => 'usewirten',
                    'newstext' => $pageContent['info']
                ]);
                file_put_contents('./youxi3500com.txt',$id."，",FILE_APPEND);
*/
            }
            //file_put_contents('./youxi3500com.txt',$category['site_name']."\n",FILE_APPEND);
        }
    }
    //获取网游
    public function getGame($url,$category)
    {
        try{
            $options = [
                'http_errors' => true,
                'force_ip_resolve' => 'v4',
                'connect_timeout' => 0,
                'read_timeout' => 0,
                'timeout' => 0,
            ];
            $response = $this->client->get($url,$options);
            $content = $response->getBody()->getContents();
            //file_put_contents('./youxi.html',$content);
            $crawler = new Crawler($content);
            return $crawler->filterXPath($category['list'])->each(function (Crawler $cr) use ($category){
                $title = $cr->filter($category['name'])->text();//标题
                $url = $cr->filter($category['content_url'])->attr('href');
                $desc = $cr->filterXPath($category['desc'])->nextAll()->text();//html/body/div[5]/div[3]/ul/li[1]/div[2]/p[2]
                $img = $cr->filter($category['img'])->attr('src');
                $img = $this->download($img);
                //var_dump($title);
                return [
                    'title' => $title,
                    'url' => $url,
                    'desc' => $desc,
                    'img' => $img,
                ];
            });
        }catch (Exception $e) {
            Log::info(date('Y-m-d H:i:s')."---{$url}页面错误，403了----");
            \Log::error($e->getMessage());
        }
        return [];
    }
    public function gamePost(int $classid, $post): void
    {
//        preg_match_all('/<img src="(.*?)"/', $post['content'], $matches);
//        foreach ($matches[1] as $item) {
//            $post['content'] = str_replace($item, $this->picDown('https://www.qiqu.net'.$item),
//                $post['content']);
//        }
//        $post['content'] = preg_replace('/<a.*?\/a>/', '', $post['content']);

        $resp = $this->client->post($this->config['postUrl'], [
            'form_params' => [
                'classid' => $classid,
                'username' => 'kaifamei',
                'title' => $post['title'],
                'titlepic' => $post['img'],
                'newstime' => '',
                'checked' => 1,
                'infotags' => $post['tags'],
                'ftitle' => $post['title'],
                'jietul' => $post['img'],
                'keyboard' => $post['tags'],
                'newstext' => $post['info'],
                'smalltext' => $post['desc'],
                'h5url' => $post['url'],
                'yijuhua' => $post['title'],
                'fenlei' => rand(1,8),
            ],
        ]);
        if (strpos($resp->getBody()->getContents(), '增加信息成功') === false) {
            echo $resp->getBody()->getContents();
            exit;
        }
    }
    public function danji()
    {
        //$url = "http://www.3500.com/newgame.html";
        $categorys = DB::table('youxi')->where('id',6)->get();
        $categorys  =json_decode($categorys,true);
        foreach ($categorys as $k=>$category){
            $link = $category['url_address'];
            $list = $this->getDanji($link,$category);
            if ($list) {
                echo '，需要更新'.count($list).'篇'.PHP_EOL;
            } else {
                echo '，没有需要更新的'.PHP_EOL;
                continue;
            }

            foreach ($list as $key=>$item){
                if($item==NULL){
                    continue;
                }
                $pageContent = $this->danjiPage($item['url']);
                //var_dump($pageContent['addr']);
                if ($pageContent === false) {
                    exit('子页面抓取错误');
                }
                if(!isset($pageContent['tags']) || $pageContent['tags']==NULL){
                    continue;
                }
                //var_dump($pageContent['info']);
                //$tag = explode(',',$pageContent['tag']);
//                $pics = $this->danjiPic($item['url']);
//                $str = '';
//                foreach ($pics as $pic){
//                    $str.=$pic['picStr'].'::::::';
//                }
                $list[$key] = array_merge($pageContent, $item);
                $classid=5;
                //array_push($list,$strpic);
                $this->danjiPost($classid,$k,$list[$key]);
/*
                $id = DB::table('www_kaifamei_com_ecms_danji')->insertGetId([
                    'classid' => 7,
                    'ttid' => 0,
                    'onclick' => 0,
                    'plnum' => 0,
                    'totaldown' => 0,
                    'userid' => 1,
                    'username' => 'kaifamei',
                    'firsttitle' => 1,
                    'isgood' => 0,
                    'ispic' => 1,
                    'istop' => 0,
                    'isqf' => 0,
                    'ismember' => 0,
                    'isurl' => 0,
                    'truetime' => time(),
                    'lastdotime' => time(),
                    'havehtml' => 1,
                    'groupid' => 0,
                    'userfen' => 0,
                    'stb' => 1,
                    'fstb' => 1,
                    'restb' => 1,
                    'keyboard' => $tag[0],
                    'title' => $item['title'],
                    'newstime' => time(),
                    'titlepic' => $item['img'],
                    'ftitle'=>$tag[0],
                    'smalltext' => $item['desc'],
                    'diggtop' => 0,
                    'fenlei' =>rand(1,11),
                    'yijuhua' =>$item['title'],
                    'morepic' => $str,
                    'yxurl'=>$item['url'],
                    'eckuid' => 0
                ]);
                DB::table('www_kaifamei_com_ecms_danji')->where('id',$id)->update([
                    'filename' => $id,
                    'titleurl' => "/danji/{$id}.html"
                ]);
                DB::table('www_kaifamei_com_ecms_danji_data_1')->insert([
                    'id' =>$id,
                    'classid' => 7,
                    'keyid' => $id,
                    'dokey' => 0,
                    'newstempid' => 0,
                    'closepl' => 0,
                    'haveaddfen' => 0,
                    //'infotage' => $tag[0],
                    //'writer' => 'usewirten',
                    'newstext' => $pageContent['info']
                ]);
                file_put_contents('./youxi3500com.txt',$id."，",FILE_APPEND);
*/
            }
            //file_put_contents('./youxi3500com.txt',$category['site_name']."\n",FILE_APPEND);
        }
    }
    //获取网游
    public function getDanji($url,$category)
    {
        try{
            $options = [
                'http_errors' => true,
                'force_ip_resolve' => 'v4',
                'connect_timeout' => 0,
                'read_timeout' => 0,
                'timeout' => 0,
            ];
            $response = $this->client->get($url,$options);
            $content = $response->getBody()->getContents();
            //file_put_contents('./youxi.html',$content);
            $crawler = new Crawler($content);

            return $crawler->filterXPath($category['list'])->each(function (Crawler $cr) use ($category){
                $title = $cr->filter($category['name'])->text();//标题
                $url = $cr->filter($category['content_url'])->attr('href');
                $desc = $cr->filterXPath($category['desc'])->nextAll()->text();//html/body/div[5]/div[3]/ul/li[1]/div[2]/p[2]
                $img = $cr->filter($category['img'])->attr('src');
                //$img = $this->download($img);
                //var_dump($title);
                return [
                    'title' => $title,
                    'url' => $url,
                    'desc' => $desc,
                    'img' => $img,
                ];
            });
        }catch (Exception $e) {
            Log::info(date('Y-m-d H:i:s')."---{$url}页面错误，403了----");
            \Log::error($e->getMessage());
        }
        return [];
    }

    public function danjiPost(int $classid, $post): void
    {
//        preg_match_all('/<img src="(.*?)"/', $post['content'], $matches);
//        foreach ($matches[1] as $item) {
//            $post['content'] = str_replace($item, $this->picDown('https://www.qiqu.net'.$item),
//                $post['content']);
//        }
//        $post['content'] = preg_replace('/<a.*?\/a>/', '', $post['content']);

        $resp = $this->client->post($this->config['postUrl'], [
            'form_params' => [
                'classid' => $classid,
                'username' => 'kaifamei',
                'title' => $post['title'],
                'titlepic' => $post['img'],
                'newstime' => time(),
                'checked' => 1,
                'infotags' => $post['tags'],
                'ftitle' => $post['title'],
                'jietul' => $post['img'],
                'keyboard' => $post['tags'],
                'newstext' => $post['info'],
                'smalltext' => $post['desc'],
                'yxurl' => $post['url'],
                'yijuhua' => $post['title'],
                'fenlei' => rand(1,8),
            ],
        ]);
        if (strpos($resp->getBody()->getContents(), '增加信息成功') === false) {
            echo $resp->getBody()->getContents();
            exit;
        }
    }



    public function gather()
    {
        /**
         * return $crawler->filterXPath("//div[@class='main']/div/ul/li")->each(function (Crawler $cr){
        $title = $cr->filter('.title')->text();//标题
        $url = $cr->filter('.con a')->attr('href');
        $desc = $cr->filterXPath("//p")->nextAll()->text();//html/body/div[5]/div[3]/ul/li[1]/div[2]/p[2]
        $img = $cr->filter('.img a img')->attr('src');
         */
        DB::table('youxi')->insert([
            'name' => '.title',
            'content_url' => '.con a',
            'desc'  => '//p',
            'img' => '.img a img',
            'list' => "//div[@class='main']/div/ul/li",
            'url_address' => "http://www.3500.com/danji.html",
            'site_name' => "小游戏",
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'site_type' => 6,//网址类型：1-黄页，2-乐宇通
        ]);
    }

    /**
     * @param  string  $url
     * @return array|false
     * @throws GuzzleException
     */
    public function page(string $url)
    {
        $info = '';
        $writer = '';//作者
        $xgname = '';//游戏名字
        try {
            $response = $this->client->request('GET', $url);
            $content = $response->getBody()->getContents();
            $crawler = new Crawler($content);
            if (preg_match('/<meta name="keywords" content="(?<tags>.*?)"/u', $content, $matches) === false) {
                return false;
            }
            //$content = $crawler->filterXPath($this->config['xpath']['body'])->html();
            if($crawler->filter('.news-con')->count()==1){
                $info = $crawler->filter('.news-con')->html();
            }else{
                $info = '';
            }
            $zuozhe= $crawler->filter('.info')->children('span')->nextAll()->text();
            $zuozhe = explode('：',$zuozhe);
            $writer = $zuozhe[1];
            $xgname = $crawler->filter('.name a')->text();
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }
        return [
            'tags' => empty($matches)?'':$matches['tags'],
            'info' => $info,
            'writer' => $writer,
            'xgname' => $xgname
        ];
    }
    public function gamePage(string $url)
    {
        $info = '';
        try {
            $response = $this->client->request('GET', $url);
            $content = $response->getBody()->getContents();
            $crawler = new Crawler($content);
            if (preg_match('/<meta name="keywords" content="(?<tags>.*?)"/u', $content, $matches) === false) {
                return false;
            }
            //$content = $crawler->filterXPath($this->config['xpath']['body'])->html();
            if($crawler->filter('.jianjie-con')->count()==1){
                $info = $crawler->filter('.jianjie-con')->html();
            }else{
                $info = '';
            }
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }
        return [
            'tags' => empty($matches)?'':$matches['tags'],
            'info' => $info,
        ];
    }

    public function gamePic($url)
    {
        $response = $this->client->request('GET', $url);
        $content = $response->getBody()->getContents();
        $crawler = new Crawler($content);
        try{
            return $crawler->filterXPath('//*[@id="showcase"]/div/div[1]/ul/li')->each(function (Crawler $node){
                $img = $node->children('img')->attr('src');
                $pic = "http://www.3500.com".$img;
                //var_dump($this->picDown($pic));
                $picStr = $this->download($pic);//.'::::::';
                return [
                    'picStr' => $picStr
                ];
            });
        }catch (Exception $e) {
            \Log::error($e->getMessage());
        }
    }
    public function danjiPage(string $url)
    {
        $info = '';
        try {
            $response = $this->client->request('GET', $url);
            $content = $response->getBody()->getContents();
            $crawler = new Crawler($content);
            if (preg_match('/<meta name="keywords" content="(?<tags>.*?)"/u', $content, $matches) === false) {
                return false;
            }
            //$content = $crawler->filterXPath($this->config['xpath']['body'])->html();
            if($crawler->filter('.jianjie-con')->count()==1){
                $info = $crawler->filter('.jianjie-con')->html();
            }else{
                $info = '';
            }
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }
        return [
            'tags' => empty($matches)?'':$matches['tags'],
            'info' => $info,
        ];
    }

    public function danjiPic($url)
    {
        $response = $this->client->request('GET', $url);
        $content = $response->getBody()->getContents();
        $crawler = new Crawler($content);
        try{
            return $crawler->filterXPath('//*[@id="showcase"]/div/div[1]/ul/li')->each(function (Crawler $node){
                $img = $node->children('img')->attr('src');
                $pic = "http://www.3500.com".$img;
                //var_dump($this->picDown($pic));
                $picStr = $this->download($pic);//
                return [
                    'picStr' => $picStr
                ];
            });
        }catch (Exception $e) {
            \Log::error($e->getMessage());
        }
    }

    /**
     * @param  string  $word  需要替换的字符串
     * @return string
     */
    public function replaceIntro(string $word): string
    {
        $word = str_ireplace('www.QiQu.net ', 'cpadol.com', $word);

        return str_replace('奇趣网', '天下奇闻网', $word);
    }

    public function replacePrice($word)
    {
        preg_match('#([+-]?\d+(\.\d+)?)#',$word,$matches);//提取数字
        return $matches[0];
    }
    public function replaceImg($word)
    {
        if($word=="//static.huangye88.cn/images/none.gif"){
            $word = "http://oss.huangye88.net/live/user/826336/1536200234001363900-0.jpg@1e_1c_98w_98h_90Q";
        }
        return $word;
    }

    public function test($word)
    {
        $replace = DB::table('replace')->where('id',1)->select('content')->first();
        $contents = explode("\r\n",$replace->content);
        foreach ($contents as $key=>$content){
            $re = explode('||',$content);
            if(!isset($re[0]) || !isset($re[1])){
                continue;
            }
            //var_dump($re[1]);
            if(strpos($word,$re[0])!==false){
                $word = str_replace($re[0],$re[1],$word);
                continue;
            }
        }
        return $word;
    }

    public function replaceAll($word)
    {
        if($word=='') return $word;
        $replace = DB::table('replace')->where('id',1)->select('content')->first();
        $contents = explode("\r\n",$replace->content);
        foreach ($contents as $key=>$content){
            $re = explode('||',$content);
            if(!isset($re[0]) || !isset($re[1])){
                continue;
            }
            //var_dump($re[1]);
            if(strpos($word,$re[0])!==false){
                $word = str_replace($re[0],$re[1],$word);
                continue;
            }
        }
        return $word;
    }

    public function download($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file = curl_exec($ch);
        curl_close($ch);
        //$filename = pathinfo($url, PATHINFO_BASENAME);
        $timePath = date('YmdHi');
        $picName = $this->randStr(11);

        if (preg_match('/^.*\.(?<extension>.*?)$/', $url, $matches) === false) {
            exit('图片扩展名获取失败');
        }
        $file = "/home/www/wannv.com/d/file/pic/".$timePath;
        if(!file_exists($file)){
            mkdir($file,0777,true);
            chmod($file,0777);
        }
        $path = $timePath.'/'.$picName.'.'.$matches['extension'];
        $resource = fopen("/home/www/wannv.com/d/file/pic/".$path, 'a');
        fwrite($resource, $file);
        fclose($resource);
        return '/d/file/pic/'.$path;
    }

    /**
     * @param  string  $url
     * @return string
     * @throws Exception
     */
    public function picDown(string $url): string
    {
        $timePath = date('YmdHis');
        $picName = $this->randStr(11);

        if (preg_match('/^.*\.(?<extension>.*?)$/', $url, $matches) === false) {
            exit('图片扩展名获取失败');
        }

        $path = $timePath.'/'.$picName.'.'.$matches['extension'];

        Storage::put($path, file_get_contents($url));

        //return config('app.url').config('youxi3500com.picPath').'/'.$path;
        return $path;
    }

    /**
     * @param  int  $length
     * @return string
     * @throws Exception
     */
    public function randStr(int $length): string
    {
        $str = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $len = strlen($str) - 1;
        $randStr = '';
        for ($i = 0; $i < $length; $i++) {
            $num = random_int(0, $len);
            $randStr .= $str[$num];
        }

        return $randStr;
    }
}
